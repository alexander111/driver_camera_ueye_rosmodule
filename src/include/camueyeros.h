#ifndef CAMUEYEROS_H
#define CAMUEYEROS_H

#include <iostream>
#include <ros/ros.h>
#include <unistd.h>

#include <iostream>
#include "CamUeye_driver.h"
#include <opencv/cv.h>
#include <image_transport/image_transport.h>
#include <opencv/cvwimage.h>
#include <cv_bridge/cv_bridge.h>
//#include <mutex>      // stdc++11
//#include <thread>     // stdc++11
#include "cvgMutex.h"
#include "cvgThread.h"
#include "droneModuleROS.h"

#include <camera_calibration_parsers/parse.h>
#include <cstdlib> // needed for getenv()
#include <sensor_msgs/CameraInfo.h>




const uint max_configuration_filepath_length = 1024;

class CamUeyeROS : public DroneModule, public virtual cvgThread {
public:
    CamUeyeROS( int hcam_ident = 1 );
    virtual ~CamUeyeROS();
    void set_config_filename( std::string config_filenamei );
    bool start();
    bool stop();
    inline float getSleepTime() {return thread_sleep_time;}
    inline bool  isStarted()    {return camera_started;}

// DroneModule
public:
    void open(ros::NodeHandle & nIn, std::string moduleName, std::string topicName=std::string());
    bool run();
protected:
    void close();
    bool resetValues();
    bool startVal();
    bool stopVal();

    //	***** controller thread related data *****
protected:
    void thread_run();
private:
    bool getImage();
    volatile bool camera_started;
    cvgMutex  camera_mutex;
    float thread_sleep_time;
    bool  got_thread_sleep_time;
    ulong last_timestamp;
    bool got_image;


    // Camera related data
    int hcam_ident_num;
    Video::CamUeye_driver myCamera;
    wchar_t config_filename[max_configuration_filepath_length];
    bool config_filename_is_set;
    int width, height;
    char *frameBuffer;
    IplImage *frame;
    int bytesperpixel, num_channels;

public:
    void loadIntrinsicsFile(std::string cam_intr_filename, std::string camera_name);
    sensor_msgs::CameraInfo ros_cam_info;
    unsigned int ros_frame_count;

 private:
    // ROS stuff
    image_transport::Publisher img_publisher;
    image_transport::CameraPublisher img_info_publisher;
    ros::Publisher camera_info_publisher;
public:
    void publishImage();
};

#endif // CAMUEYEROS_H
