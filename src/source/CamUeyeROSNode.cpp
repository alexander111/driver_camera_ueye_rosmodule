/*
 * main.cpp
 *
 *  Created on: 09/01/2013
 *      Author: Jesus Pestana
 */

#include "camUeye_defines.h"
#include <iostream>
#include <ros/ros.h>
#include <ros/param.h>
#include "camueyeros.h"
#include "stdexcept"
#include "Timer.h"
#include "nodes_definition.h"

int main( int argc, char **argv) {
    //Ros Init
    ros::init(argc, argv, "cvg_ueye_cam");
    ros::NodeHandle nh;

    // read external parameters
    int hCam_ident = 1; // camera identification number, as shown in the ueyecameramanager
    std::string config_file;
    std::string camera_name;
    std::string topic_name;
    std::string cam_intr_filename;
    ros::param::get("~camera_ident_number",hCam_ident);
    ros::param::get("~camera_name",camera_name);
    ros::param::get("~topic_name",topic_name);
    ros::param::get("~configuration_file", config_file);
    ros::param::get("~cam_intr_filename",cam_intr_filename);
    if ( config_file.length() == 0 ) {
        config_file = "/home/asctec/cvg_ros_workspace/stack/driver_camera_ueye/configs/small_camera_RGB24.ini";
    }

    if (cam_intr_filename.length() <= 0) { // Use default filename
    cam_intr_filename = std::string(getenv("HOME")) + "/.ros/camera_info/"+camera_name+".yaml";
    }


    std::cout << "hCam_ident:"        << hCam_ident  << std::endl;
    std::cout << "config_file:"       << config_file.c_str() << std::endl;
    std::cout <<"[ROSNODE] Starting " << camera_name << " " << MODULE_NAME_UEYE_CAMERA << hCam_ident << std::endl;
    std::cout <<"cam_intr_file "       << cam_intr_filename << std::endl;

    CamUeyeROS my_camueye_ros(hCam_ident);

    my_camueye_ros.loadIntrinsicsFile(cam_intr_filename, camera_name);
    my_camueye_ros.set_config_filename(config_file);
    {
        StringStacker module_name_str_stk;
        module_name_str_stk << MODULE_NAME_UEYE_CAMERA << hCam_ident;
        my_camueye_ros.open( nh, module_name_str_stk.getStackedString(), topic_name);
    }

    try
    {
        while(ros::ok())
        {
            //Read messages
            ros::spinOnce();

            //Run retina
            if(my_camueye_ros.run()) {}

            //Sleep
            my_camueye_ros.sleep();
        }
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

    return 0;
}


